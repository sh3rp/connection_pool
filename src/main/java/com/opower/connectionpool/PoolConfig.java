package com.opower.connectionpool;

/**
 * Simple POJO to carry configuration payload.  Configuration
 * is loaded from a properties file.
 * 
 * @author skendall
 *
 */

public class PoolConfig {
	
	// database related configuration
	
	private String sqlDriver;
    private String connectString;
	private String username;
	private String password;
	
	// pool related configuration
	
	public static final int DEFAULT_MIN_POOL_SIZE = 5;
	public static final int DEFAULT_MAX_POOL_SIZE = 10;
	
	private int minPoolSize = DEFAULT_MIN_POOL_SIZE;
	private int maxPoolSize = DEFAULT_MAX_POOL_SIZE;
	private int connectionTimeout = 20000;

    /** ran out of time getting a new connection **/

    private int pollTimeout = 1000;

    /** lazy load connections to database **/

    private boolean lazyConnection = false;

    public String getSqlDriver() {
        return sqlDriver;
    }

    public void setSqlDriver(String sqlDriver) {
        this.sqlDriver = sqlDriver;
    }

    public String getConnectString() {
        return connectString;
    }

    public void setConnectString(String connectString) {
        this.connectString = connectString;
    }

    public int getPollTimeout() {
        return pollTimeout;
    }

    public void setPollTimeout(int pollTimeout) {
        this.pollTimeout = pollTimeout;
    }

    public boolean isLazyConnection() {
        return lazyConnection;
    }

    public void setLazyConnection(boolean lazyConnection) {
        this.lazyConnection = lazyConnection;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(int minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }
}
