package com.opower.connectionpool.impl;

import com.opower.connectionpool.ConnectionPool;
import com.opower.connectionpool.PoolConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class DefaultConnectionPool implements ConnectionPool {

    private Queue<InternalConnection> available;
    private Map<Integer,InternalConnection> used;

    private PoolConfig config;

    private DefaultConnectionPool(PoolConfig config) {
        this.config = config;
        this.available = new ArrayBlockingQueue<InternalConnection>(config.getMaxPoolSize());
        this.used = new HashMap<Integer,InternalConnection>();
        init();
    }

    private void init() {
        try {
            Class.forName(config.getSqlDriver());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        for(int i=0;i<config.getMinPoolSize();i++) {
            InternalConnection connection = newConnection();
            available.offer(connection);
        }
    }

    private InternalConnection newConnection() {
        InternalConnection internalConnection = null;
        try {
            Connection connection = DriverManager.getConnection(config.getConnectString() + "?");
            internalConnection = new InternalConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return internalConnection;
    }

	public Connection getConnection() throws SQLException {
        InternalConnection connection = null;

        // if no connections available and still under pool size, create a new
        // connection

        if(available.size() == 0) {
            if(used.size() < config.getMaxPoolSize())
                connection = newConnection();
            else
                throw new SQLException("Connection pool is full, no available connections. (" + used.size() + " connections)");
        } else {
            connection = available.poll();
        }
        if(connection == null)
            throw new SQLException("Connection was null.");
        used.put(connection.hashCode(),connection);
        return connection.lock().getConnection();
	}

	public void releaseConnection(Connection connection) throws SQLException {
        InternalConnection c = used.get(connection.hashCode());
        c.unlock();
        used.remove(connection.hashCode());
        available.offer(c);
	}

    private class InternalConnection {
        private Connection connection;
        private ReentrantLock lock;

        public InternalConnection(Connection connection) {
            this.connection = connection;
            this.lock = new ReentrantLock();
        }

        private Connection getConnection() {
            return connection;
        }

        private void setConnection(Connection connection) {
            this.connection = connection;
        }

        private InternalConnection lock() {
            this.lock.lock();
            return this;
        }

        private InternalConnection unlock() {
            this.lock.unlock();
            return this;
        }

        public int hashCode() {
            return connection.hashCode();
        }
    }

}
