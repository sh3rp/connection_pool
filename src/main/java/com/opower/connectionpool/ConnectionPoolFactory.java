package com.opower.connectionpool;

public class ConnectionPoolFactory {
	private static final ConnectionPoolFactory INSTANCE = new ConnectionPoolFactory();
	
	private ConnectionPoolFactory() { }
	
	public static ConnectionPoolFactory getInstance() {
		return INSTANCE;
	}
	
	public ConnectionPool newConnectionPool(PoolConfig configuration) {
		return null;
	}
	
}
